package com.andredezzy.core.util;

import java.text.DecimalFormat;

public class Mathematical {

    public double trim(int suffixAmount, double value) {
        StringBuilder sa = new StringBuilder();
        for (int i = 0; i < suffixAmount; i++) sa.append("#");

        DecimalFormat df = new DecimalFormat(String.format("#.%s", sa.toString()));
        return Double.parseDouble(df.format(value).replaceAll(",", "."));
    }

    public int toMegaBytes(long bytes) {
        int div = 1024 * 1024;
        return Math.toIntExact(bytes / div);
    }
}
