package com.andredezzy.core.util.machine.ram;

import com.andredezzy.core.util.Formatter;
import com.andredezzy.core.util.Mathematical;

import java.text.DecimalFormat;

public class Ram {

    private final DecimalFormat percentageFormat;
    private final int max;
    private final int used;
    private final String percentage;
    private final String graph;

    public Ram(int used, int max) {
        Mathematical math = new Mathematical();
        Formatter formatter = new Formatter();

        this.percentageFormat = new DecimalFormat("###,##0.0");
        this.max = max;
        this.used = used;
        this.percentage = this.percentageFormat.format((used * 100.0F) / max).replaceAll(",", ".");
        this.graph = formatter.getProgressBar(this.used, this.max);
    }

    public int max() {
        return max;
    }

    public int used() {
        return used;
    }

    public String percentage() {
        return percentage;
    }

    public String graph() {
        return graph;
    }
}
