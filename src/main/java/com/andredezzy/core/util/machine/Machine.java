package com.andredezzy.core.util.machine;

import com.andredezzy.core.util.Mathematical;
import com.andredezzy.core.util.TimeConverter;
import com.andredezzy.core.util.machine.ram.Ram;

public class Machine {

    public final String os;
    public final String osVersion;
    public final String osArchitecture;
    public final int coreAmount;
    private final long startTime;

    public Machine() {
        this.os = System.getProperty("os.name", "generic");
        this.osVersion = System.getProperty("os.version");
        this.osArchitecture = System.getProperty("os.arch");
        this.coreAmount = Runtime.getRuntime().availableProcessors();
        this.startTime = System.currentTimeMillis();
    }

    public Ram getRam() {
        Mathematical math = new Mathematical();
        int used = math.toMegaBytes(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
        int max = math.toMegaBytes(Runtime.getRuntime().maxMemory());

        return new Ram(used, max);
    }

    public String uptime(){
        return TimeConverter.unconvert(System.currentTimeMillis() - startTime);
    }
}
