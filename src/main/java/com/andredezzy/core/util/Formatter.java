package com.andredezzy.core.util;

public class Formatter {

    public String getProgressBar(int first, int second) {
        int percentage = (int) Math.floor(100.0 * first / second);

        int width = percentage > 0 ? percentage / 5 : 0;
        StringBuilder sb = new StringBuilder();

        if (width > 0) sb.append("§c");

        for (int i = 0; i < 20; ++i) {
            if (i == width) sb.append("§a");
            sb.append("|");
        }
        return sb.toString();
    }
}
