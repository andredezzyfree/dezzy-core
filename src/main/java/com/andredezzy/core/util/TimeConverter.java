package com.andredezzy.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
 * @author: yExtremeDev
 * github repository: https://github.com/yExtremeDev/TimeConverter/
 */
public class TimeConverter {

    public static long convert(String convert) {
        String[] split = convert.split(" ");
        for (int i = 0; i != split.length; i++) split[i] = split[i].replace(",", "").replace(" ", "");
        long finalLong = 0L;
        int i = 0;
        while (i <= split.length) {
            try {
                String stringlong = split[i];
                String stringunit = split[++i];
                long l = Long.parseLong(stringlong);
                TimeUnit unit = TimeUnit.get(stringunit.toLowerCase());
                if (unit != null)
                    finalLong += unit.getTime(l);
            } catch (ArrayIndexOutOfBoundsException | NumberFormatException ignored) {
            }
            i++;
        }
        return finalLong;
    }

    public static String unconvert(long unconvert, int limit) {
        List<String> strings = new ArrayList<>();
        while (unconvert >= 1000 && limit != 0) {
            TimeUnit unit = TimeUnit.getHigherValue(unconvert);
            if (unit != null) {
                int get = (int) (unconvert / unit.getTime());
                unconvert %= unit.getTime();
                strings.add(String.format("%s", unit.getValueFrom(get)));
                limit--;
            }
        }
        StringBuilder returnable = new StringBuilder();
        if (strings.isEmpty())
            returnable.append("0 segundos");
        else
            for (int i = 0; i != strings.size(); i++)
                returnable.append(strings.get(i)).append(i + 2 == strings.size() ? " e " : (i + 1 != strings.size() ? ", " : ""));
        return returnable.toString();
    }

    public static String unconvert(long unconvert) {
        return unconvert(unconvert, TimeUnit.values().length);
    }

    public enum TimeUnit {

        ANOS(31556952000L, "ano", "year", "y", "a"),
        MESES(2592000000L, "mês", "mes", "months", "month", "m"),
        SEMANAS(604800000L, "semana", "weeks", "week", "w", "s"),
        DIAS(86400000L, "dia", "day", "days", "d"),
        HORAS(3600000L, "hora", "hour", "hours", "h"),
        MINUTOS(60000L, "minuto", "minute", "minutes", "min"),
        SEGUNDOS(1000L, "segundo", "seconds", "second", "seg", "sec");

        private final long time;
        private final String[] aliases;

        TimeUnit(long time, String... alises) {
            this.time = time;
            this.aliases = alises;
        }

        public static TimeUnit get(String string) {
            return Arrays.stream(values()).filter(unit -> Arrays.stream(unit.getAliases()).anyMatch(aliases -> aliases.equalsIgnoreCase(string)) || string.equalsIgnoreCase(unit.name())).findFirst().orElse(null);
        }

        public static TimeUnit getHigherValue(long value) {
            return Arrays.stream(values()).filter(unit -> unit.getTime() <= value).findFirst().orElse(null);
        }

        public String[] getAliases() {
            return aliases;
        }

        public long getTime(long time) {
            return this.time * time;
        }

        public long getTime() {
            return time;
        }

        public String getSingularName() {
            return aliases[0];
        }

        public String getPluralName() {
            return name().toLowerCase();
        }

        public String getValueFrom(long value) {
            return value + " " + (value == 1 ? getSingularName() : getPluralName());
        }
    }
}
