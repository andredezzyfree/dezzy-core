package com.andredezzy.core.command;

import com.andredezzy.core.init.Core;
import org.bukkit.command.CommandSender;

import java.util.List;

public abstract class Command {

    public final Core core;

    public Command(Core core) {
        this.core = core;
    }

    public abstract String getName();

    public abstract List<String> getAliases();

    public abstract String getDescription();

    public abstract boolean canBeExecutedByConsole();

    public abstract void execute(CommandSender sender, String[] args);
}
