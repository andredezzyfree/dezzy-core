package com.andredezzy.core.command;

import com.andredezzy.core.init.Core;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.Field;

public class CommandManager {

    private final Core core;

    public static final String COMMAND_CANNOT_BE_EXECUTED_BY_CONSOLE = "§cApenas jogadores podem executar este comando.";

    public CommandManager(Core core) {
        this.core = core;
    }

    public void registerCommand(Command command) {
        org.bukkit.command.Command registering = new org.bukkit.command.Command(command.getName()) {
            @Override
            public boolean execute(CommandSender sender, String label, String[] args) {
                if (!(sender instanceof Player)) {
                    if (command.canBeExecutedByConsole()) {
                        command.execute(sender, args);
                    } else {
                        sender.sendMessage(COMMAND_CANNOT_BE_EXECUTED_BY_CONSOLE);
                    }
                    return true;
                }
                command.execute(sender, args);
                return false;
            }
        };

        if (command.getAliases() != null) registering.setAliases(command.getAliases());
        getCommandMap().register(command.getName(), registering);

        getCore().log("Command", String.format("Registered command: %s", command.getName()));
    }

    public void unregisterCommand(Command command) {
        org.bukkit.command.Command cmd = getCommandMap().getCommand(command.getName());
        if (cmd != null) {
            getCore().log("Command", String.format("Unregistered command: %s", command.getName()));
        } else
            getCore().log("Command", String.format("Tried to unregister command '%s', but he isn't registered.", command.getName()));
    }

    private static CommandMap getCommandMap() {
        CommandMap commandMap = null;
        try {
            if (Bukkit.getPluginManager() instanceof SimplePluginManager) {
                Field f = SimplePluginManager.class.getDeclaredField("commandMap");
                f.setAccessible(true);

                commandMap = (CommandMap) f.get(Bukkit.getPluginManager());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return commandMap;
    }

    public Core getCore() {
        return core;
    }
}
