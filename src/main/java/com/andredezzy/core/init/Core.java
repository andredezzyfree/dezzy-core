package com.andredezzy.core.init;

import com.andredezzy.core.command.CommandManager;
import com.andredezzy.core.recipe.RecipeManager;
import com.andredezzy.core.recipe.listener.CraftingItem;
import com.andredezzy.core.util.Mathematical;
import com.andredezzy.core.util.machine.Machine;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class Core extends JavaPlugin implements Plugin {

    private final Machine machine;
    private final CommandManager commandManager;
    private RecipeManager recipeManager;

    public Core() {
        this.machine = new Machine();
        this.commandManager = new CommandManager(this);
    }

    @Override
    public void onLoad() {
        log("Core", "Starting...");
        onStartup();
    }

    @Override
    public void onEnable() {
        this.recipeManager = new RecipeManager(this);
        log("Core", "Running...");
        onRun();
    }

    @Override
    public void onDisable() {
        log("Core", "Stopping...");
        onStop();
    }

    public void log(String prefix, String message) {
        getServer().getConsoleSender().sendMessage(String.format("§e[%s] %s", prefix, message));
    }

    public Machine getMachine() {
        return machine;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public RecipeManager getRecipeManager() {
        return recipeManager;
    }

    public String getTps(ChatColor color) {
        StringBuilder sb = new StringBuilder(color.toString());
        Mathematical math = new Mathematical();

        int i = 0;
        double[] recentTps = MinecraftServer.getServer().recentTps;

        for (double tps : recentTps) {
            i++;
            sb.append(math.trim(1, tps));
            if (i < recentTps.length) sb.append(String.format("§f, %s", color.toString()));
        }
        return sb.toString();
    }
}
