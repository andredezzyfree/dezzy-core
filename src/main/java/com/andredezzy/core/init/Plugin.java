package com.andredezzy.core.init;

public interface Plugin {

    void onStartup();

    void onRun();

    void onStop();
}
