package com.andredezzy.core.recipe;

import com.andredezzy.core.init.Core;
import com.andredezzy.core.recipe.ingredient.IngredientBuilder;
import com.andredezzy.core.util.Reflection;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.*;
import java.util.stream.Collectors;

public class RecipeManager {

    private Core core;
    public Map<ItemStack, List<Recipe>> recipes;

    public RecipeManager(Core core) {
        this.core = core;
        this.recipes = new HashMap<>();
    }

    public void registerRecipe(CustomRecipe recipe) {
        try {
            ItemStack result = recipe.getResult();
            Reflection.getField(ShapedRecipe.class, "output").set(recipe, result);

            Bukkit.getConsoleSender().sendMessage("§2" + recipe.getCraft().getShape());
            recipe.shape(recipe.getShape());

            for (IngredientBuilder ingredient : recipe.getIngredients()) {
                recipe.setIngredient(ingredient.getChar(), ingredient.getItem());
            }

            List<Recipe> list = this.recipes.get(result);
            if (list == null) list = new ArrayList<>();

            if (!list.contains(recipe)) {
                list.add(recipe);
                this.recipes.put(result, list);
            }

            this.core.getServer().addRecipe(recipe);
            this.core.log("Recipe", String.format("Registered a new custom recipe §f%s§e, result: §f%s§e.", Arrays.toString(recipe.getCraft().getShape()),
                    ChatColor.stripColor(recipe.getResult().getItemMeta().getDisplayName())));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public List<Recipe> getRecipesByItem(ItemStack item) {
        if (item != null) {
            for (Map.Entry<ItemStack, List<Recipe>> recipe : recipes.entrySet()) {
                ItemStack result = recipe.getKey();
                if (result.getItemMeta() instanceof SkullMeta) {
                    SkullMeta resultMeta = (SkullMeta) result.getItemMeta();
                    if (item.hasItemMeta()) {
                        SkullMeta itemMeta = (SkullMeta) item.getItemMeta();
                        if (resultMeta.getOwner().equalsIgnoreCase(itemMeta.getOwner())) {
                            return recipe.getValue();
                        }
                    }
                } else {
                    if (result.isSimilar(item)) {
                        return recipe.getValue();
                    }
                }
            }
        }
        return null;
    }
}
