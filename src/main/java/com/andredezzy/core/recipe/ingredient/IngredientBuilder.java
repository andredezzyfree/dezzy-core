package com.andredezzy.core.recipe.ingredient;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

public class IngredientBuilder {

    private char c;
    private ItemStack item;
    private ChatColor squareColor;

    public IngredientBuilder(char c, ItemStack item, ChatColor squareColor) {
        this.c = c;
        this.item = item;
        this.squareColor = squareColor;
    }

    public char getChar() {
        return c;
    }

    public ItemStack getItem() {
        return item;
    }

    public ChatColor getSquareColor() {
        return squareColor;
    }
}
