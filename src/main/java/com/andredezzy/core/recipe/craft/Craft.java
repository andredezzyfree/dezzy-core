package com.andredezzy.core.recipe.craft;

import com.andredezzy.core.recipe.CustomRecipe;
import com.andredezzy.core.recipe.anns.Ingredient;
import com.andredezzy.core.recipe.ingredient.IngredientBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.*;

public class Craft {

    private static final char[] shapeIds = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};

    public List<IngredientBuilder> ingredients;
    private Map<IngredientBuilder, Integer> ingredientsAmount;

    private String[] shape;
    private String colors;
    private String table;
    private String items;

    public Craft(CustomRecipe recipe) {
        this.ingredients = new ArrayList<>();
        this.ingredientsAmount = new HashMap<>();

        StringBuilder shape = new StringBuilder("         ");
        try {
            int idCount = 0;
            for (Field f : recipe.getClass().getDeclaredFields()) {
                Ingredient ingredient = f.getAnnotation(Ingredient.class);
                if (ingredient != null) {
                    f.setAccessible(true);

                    ItemStack item = (ItemStack) f.get(recipe);
                    char charId = shapeIds[idCount];

                    ingredients.add(new IngredientBuilder(charId, item, ingredient.color()));
                    for (int id : ingredient.slot()){
                        shape.setCharAt(id, charId);
                    }
                    idCount++;
                }
            }
            shape.insert(3,"|").insert(7, "|");
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.shape = shape.toString().split("\\|");
        Bukkit.getConsoleSender().sendMessage("§1§l" + Arrays.toString(this.shape));
        this.colors = getIngredientsColor();
        this.table = getCraftTable();
        this.items = getIngredientsAmount();
    }

    public String[] getShape() {
        return shape;
    }

    public String getColors() {
        return colors;
    }

    public String getCraftingTable() {
        return table;
    }

    public String getItems() {
        return items;
    }

    private IngredientBuilder ingredientByChar(char c) {
        for (IngredientBuilder ingredient : ingredients) {
            if (ingredient.getChar() == c) {
                return ingredient;
            }
        }
        return null;
    }

    private String getIngredientsColor() {
        StringBuilder colorsBuilder = new StringBuilder("§8█ = Vázio\n");
        for (IngredientBuilder ingredient : ingredients) {
            String square = String.format("%s█", ingredient.getSquareColor());
            String itemName = ChatColor.stripColor(ingredient.getItem().getItemMeta().getDisplayName());
            colorsBuilder.append(String.format("%s = %s\n", square, itemName));
        }
        return colorsBuilder.toString();
    }

    private String getIngredientsAmount() {
        StringBuilder itemsBuilder = new StringBuilder();
        for (Map.Entry<IngredientBuilder, Integer> ingredient : ingredientsAmount.entrySet()) {
            String itemName = ingredient.getKey().getItem().getItemMeta().getDisplayName();
            itemsBuilder.append(String.format("§7- %sx %s\n", ingredient.getValue(), ChatColor.stripColor(itemName)));
        }
        return itemsBuilder.toString();
    }

    private String getCraftTable() {
        StringBuilder squaresBuilder = new StringBuilder();
        for (String row : getShape()) {
            for (int s = 0; s != row.length(); s++) {
                char charAt = row.charAt(s);
                if (charAt == ' ') {
                    squaresBuilder.append("§8█");
                    continue;
                }
                IngredientBuilder ingredient = ingredientByChar(charAt);
                if (ingredient != null) {
                    int ingredientAmount;
                    if (this.ingredientsAmount.get(ingredient) != null) {
                        ingredientAmount = this.ingredientsAmount.get(ingredient);
                    } else ingredientAmount = 0;

                    this.ingredientsAmount.put(ingredient, ++ingredientAmount);
                    squaresBuilder.append(String.format("%s█", ingredient.getSquareColor()));
                }
            }
            squaresBuilder.append("\n");
        }
        return squaresBuilder.toString();
    }
}
