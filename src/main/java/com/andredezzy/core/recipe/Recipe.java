package com.andredezzy.core.recipe;

import com.andredezzy.core.init.Core;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.material.MaterialData;

import java.util.HashMap;
import java.util.Map;

public abstract class Recipe extends ShapedRecipe implements Listener {

    private Core core;
    private Map<Character, ItemStack> exactIntegrients = new HashMap<Character, ItemStack>();

    public Recipe() {
        super(new ItemStack(Material.ANVIL));
    }

    @Override
    public Recipe setIngredient(char key, Material ingredient) {
        super.setIngredient(key, ingredient);
        this.exactIntegrients.put(key, new ItemStack(ingredient));
        return this;
    }

    @Override
    public Recipe setIngredient(char key, MaterialData ingredient) {
        super.setIngredient(key, ingredient);
        this.exactIntegrients.put(key, new ItemStack(ingredient.getItemType(), 1, ingredient.getData()));
        return this;
    }

    @Override
    public Recipe setIngredient(char key, Material ingredient, int raw) {
        super.setIngredient(key, ingredient, raw);
        this.exactIntegrients.put(key, new ItemStack(ingredient, 1, (short) raw));
        return this;
    }

    public Recipe setIngredient(char key, ItemStack item) {
        super.setIngredient(key, item.getType(), item.getDurability());
        this.exactIntegrients.put(key, item);
        return this;
    }

    public boolean equalsMatrix(ItemStack[] matrix) {
        String[] shape = super.getShape();
        for (int y = 0; y < shape.length; y++) {
            String line = shape[y];
            for (int x = 0; x < line.length(); x++) {
                char c = line.charAt(x);
                int i = y * line.length() + x;
                ItemStack matrixIngredient = matrix[i];
                ItemStack recipeIngredient = this.exactIntegrients.get(c);
                if (matrixIngredient != null && recipeIngredient != null) {
                    if (matrixIngredient.isSimilar(recipeIngredient)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
