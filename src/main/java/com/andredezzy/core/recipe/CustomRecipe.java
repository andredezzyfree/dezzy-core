package com.andredezzy.core.recipe;

import com.andredezzy.core.init.Core;
import com.andredezzy.core.recipe.anns.Result;
import com.andredezzy.core.recipe.craft.Craft;
import com.andredezzy.core.recipe.ingredient.IngredientBuilder;
import com.andredezzy.core.recipe.listener.CraftingItem;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

public abstract class CustomRecipe extends Recipe {

    private Craft craft;

    public CustomRecipe(Core core) {
        core.getServer().getPluginManager().registerEvents(new CraftingItem(core), core);
    }

    public ItemStack getResult() {
        try {
            for (Field f : getClass().getDeclaredFields()) {
                Result ingredient = f.getAnnotation(Result.class);
                if (ingredient != null) {
                    f.setAccessible(true);
                    if (f.get(this) instanceof ItemStack) {
                        return (ItemStack) f.get(this);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Craft getCraft() {
        if (this.craft == null) {
            this.craft = new Craft(this);
        }
        return this.craft;
    }

    @Override
    public String[] getShape() {
        return getCraft().getShape();
    }

    public List<IngredientBuilder> getIngredients() {
        return getCraft().ingredients;
    }
}
