package com.andredezzy.core.recipe.listener;

import com.andredezzy.core.init.Core;
import com.andredezzy.core.recipe.Recipe;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

public class CraftingItem implements Listener {

    private Core core;

    public CraftingItem(Core core) {
        this.core = core;
    }

    @EventHandler
    public void onPrepareItemCraft(PrepareItemCraftEvent event) {
        CraftingInventory craftInventory = event.getInventory();

        ItemStack result = craftInventory.getResult();
        List<Recipe> recipes = this.core.getRecipeManager().getRecipesByItem(result);

        if (recipes != null) {
            craftInventory.setResult(null);
            for (Recipe recipe : recipes) {
                if (recipe.equalsMatrix(craftInventory.getMatrix())) {
                    craftInventory.setResult(recipe.getResult());
                }
            }
        }
    }
}
