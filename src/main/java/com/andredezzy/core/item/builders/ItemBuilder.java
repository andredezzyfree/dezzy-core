package com.andredezzy.core.item.builders;

import com.andredezzy.core.item.Builder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder extends Builder<ItemBuilder, ItemMeta> {

    public ItemBuilder(Material material) {
        super(material);
    }

    public ItemBuilder(ItemStack item) {
        super(item);
    }

    public SkullBuilder asSkull() {
        return new SkullBuilder(this);
    }

    @Override
    protected ItemBuilder getThis() {
        return this;
    }

}

