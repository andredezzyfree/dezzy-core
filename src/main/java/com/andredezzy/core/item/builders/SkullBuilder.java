package com.andredezzy.core.item.builders;

import com.andredezzy.core.item.Builder;
import com.andredezzy.core.util.Reflection;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.properties.PropertyMap;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.UUID;

public class SkullBuilder extends Builder<SkullBuilder, SkullMeta> {

    public SkullBuilder() {
        super(new ItemStack(Material.SKULL_ITEM, 1, (short) 3));
    }

    public SkullBuilder(Builder builder) {
        super(builder);
    }

    public SkullBuilder owner(String name) {
        meta.setOwner(name);
        return this;
    }

    public SkullBuilder textures(String textures) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        PropertyMap propertyMap = profile.getProperties();
        if (propertyMap != null) {
            byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", textures).getBytes());
            propertyMap.put("textures", new Property("textures", new String(encodedData)));
            Reflection.getField(meta.getClass(), "profile", GameProfile.class).set(meta, profile);
        }
        return this;
    }

    @Override
    public SkullBuilder removeAttributes() {
        return this;
    }

    @Override
    protected SkullBuilder getThis() {
        return this;
    }
}
