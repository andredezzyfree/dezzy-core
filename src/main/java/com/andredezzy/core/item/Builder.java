package com.andredezzy.core.item;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;


public abstract class Builder<GenericBuilder extends Builder, GenericMeta extends ItemMeta> {

    protected GenericMeta meta;
    private ItemStack item;

    public Builder(Material material) {
        this(new ItemStack(material));
    }

    public Builder(ItemStack item) {
        this(item, (GenericMeta) item.getItemMeta());
    }

    public Builder(Builder builder) {
        this(builder.item, (GenericMeta) builder.meta);
    }

    protected Builder(ItemStack item, GenericMeta meta) {
        this.item = item;
        this.meta = meta;
    }

    public GenericBuilder amount(int amount) {
        item.setAmount(amount);
        return getThis();
    }

    public GenericBuilder durability(short data) {
        item.setDurability(data);
        return getThis();
    }

    public GenericBuilder data(short data) {
        item.setDurability(data);
        return getThis();
    }

    public GenericBuilder enchantment(Enchantment enchantment, int level) {
        item.addUnsafeEnchantment(enchantment, level);
        return getThis();
    }

    public GenericBuilder enchantments(Map<Enchantment, Integer> enchantments) {
        item.addUnsafeEnchantments(enchantments);
        return getThis();
    }

    public GenericBuilder name(String name) {
        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        return getThis();
    }


    public GenericBuilder lore(String lore) {
        meta.setLore(Collections.singletonList(lore));

        return getThis();
    }

    public GenericBuilder lore(String... lore) {
        meta.setLore(colorizeLore(Arrays.asList(lore)));

        return getThis();
    }

    public GenericBuilder lore(List<String> lore) {
        meta.setLore(colorizeLore(lore));

        return getThis();
    }

    public GenericBuilder flags(ItemFlag... flags) {
        meta.addItemFlags(flags);
        return getThis();
    }

    public GenericBuilder removeAttributes() {
        meta.addItemFlags(ItemFlag.values());
        item.setItemMeta(meta);
        return getThis();
    }

    public ItemStack build() {
        item.setItemMeta(meta);
        return item;
    }


    protected abstract GenericBuilder getThis();

    private List<String> colorizeLore(List<String> list) {
        List<String> l = new ArrayList<>();
        for (String s : list) {
            l.add(ChatColor.translateAlternateColorCodes('&', s));
        }
        return l;
    }

}
